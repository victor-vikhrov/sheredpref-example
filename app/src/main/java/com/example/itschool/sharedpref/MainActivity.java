package com.example.itschool.sharedpref;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private Button bClick, bLoad;
    private EditText etInput;
    private TextView tScore;

    private SharedPreferences preferences;
    private static final String KEY_PREF = "save";
    private static final String MY_DATA = "data";

    private String username;
    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bLoad = (Button) findViewById(R.id.b_load);
        bClick = (Button) findViewById(R.id.b_click);
        bLoad.setOnClickListener(this);
        bClick.setOnClickListener(this);
        etInput = (EditText) findViewById(R.id.et_input);
        tScore = (TextView) findViewById(R.id.tv_counter);
        preferences = getSharedPreferences(KEY_PREF, MODE_PRIVATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(username, counter);
        editor.commit();
        editor.apply();
    }

    @Override
    public void onClick(View view) {
        username = etInput.getText().toString();

        switch (view.getId()) {
            case R.id.b_load :
                if (!username.equals("")) {
                    counter = preferences.getInt(username, 0);
                }
                tScore.setText(String.valueOf(counter));
                break;
            case R.id.b_click:
                counter++;
                tScore.setText(String.valueOf(counter));
                break;

        }
    }
}
